#ifndef POLCAVOJ_COMMAND_H
#define POLCAVOJ_COMMAND_H

#include <iostream>
#include <string>
#include "../pom/constants.h"

using namespace std;

/**
 * This class is abstract and it's used as a parent for most commands.
 */

class CCommand{
    public:
        /**
         * Constructor, which sets variable m_type. It's always called from a descendant.
         *
         * @param[in] type Type of mode in which is program currently running.
         */
        CCommand( const string & type ) : m_type(type){};

        /**
         * Destructor, used in some descendants.
         */
        virtual ~CCommand(){};

        /**
         *  Method which uses polymorphism. Behavior of this method depends on descendant.
         */
        virtual void executeCommand() = 0;

        /**
         * Method which uses polymorphism. Behavior of this method depends on descendant.
         *
         * @param[in,out] os Ostream for printing to command line.
         */
        virtual void print( ostream & os ) const = 0;

        /**
         * Friend function, which overloads operator <<.
         *
         * @param[in] os Ostream for printing on command line.
         * @param[in] com Constant object CCommand.
         *
         * @return Printed ostream on command line.
         */
        friend ostream & operator<<( ostream & os, const CCommand & com );

    protected:
        /** Type of mode in which program currently running. */
        string m_type;

};

#endif //POLCAVOJ_COMMAND_H
