#include "client.h"

CClient::~CClient( void ){
    close( m_sock );
}

bool CClient::isRunning( void ){
    if( ! m_running ) return false;
    return true;
}

void CClient::treatResponse(  ){
    bool flag = false;
    int cntBytes;
    char previousByte, actualByte;
    string text = "", pom = "";
    while (1) {
        cntBytes = recv(m_sock, m_buffer, sizeof(m_buffer - 1), 0);

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                cout << text << endl;
                pom += text;
                text = "";
                flag = true;
            }
            else
                text += m_buffer[i];

            previousByte = actualByte;
        }

        if ( flag ) {
            flag = false;
            break;
        }
    }
    if( pom == LOGIN_FAILED )
        m_running = false;
}

bool CClient::authenticate(){
    int status = 0;
    string command = "", message = "";
    while( 1 ) {
        treatResponse();
        if( ! m_running ){
            send( m_sock, SRV_QUIT, 6, 0);
            treatResponse();
            return false;
        }
        status++;

        if (status == 1) {
            cout << "User: ";
            cin >> command;
            message += SRV_USER + command + "\r\n";
            send(m_sock, message.c_str(), message.length(), 0);
        }
        else if (status == 2){
            cout << "Password: ";
            cin >> command;
            message += SRV_PASS + command + "\r\n";
            send( m_sock, message.c_str(), message.length(), 0);
        }
        else if( status == 3 )
            return true;

        message = "";
    }
}

void CClient::communicate(){
    string command = "";

    fd_set rd;
    FD_ZERO ( &rd );
    FD_SET ( m_sock, &rd );

    if( ! authenticate() ){
        getline( cin, command );
        return;
    }

    getline( cin, command );
    while( 1 ){
        cout << "ftp->";
        getline( cin, command );
        if( command == COMMAND_CLOSE ){
            send( m_sock, SRV_QUIT, 6, 0);
            treatResponse();
            break;
        }
        else if( command == COMMAND_HELP )
            cout << HELP;

    }
}