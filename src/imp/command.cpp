#include "command.h"

ostream & operator<<( ostream & os, const CCommand & com ){
    com.print(os);
    return os;
}