#ifndef POLCAVOJ_HELP_H
#define POLCAVOJ_HELP_H

#include "../imp/command.h"

#include <iostream>
#include <string>

/**
 * This class represents command help. It's descendant of CCommand.
 *
 * Class display a list of available commands to the command line.
 */

class CHelp : public CCommand{
    public:
        /**
         * Constructor in which is called constructor of parent.
         *
         * @param[in] type Type of mode in which is program currently running.
         */
        CHelp( const string & type ) : CCommand( type ){};

        /**
         * Destructor.
         */
        virtual ~CHelp(){};

        /**
         * Method calls overloaded operator <<. In serve just for printing.
         */
        virtual void executeCommand();

        /**
         * Method display all available commands, which are depending on the currently mode.
         *
         * @param[in] os Ostream which is printed to the command line.
         */
        virtual void print( ostream &  os ) const;

    protected:
};

#endif //POLCAVOJ_HELP_H
