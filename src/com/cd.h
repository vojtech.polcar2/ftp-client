#ifndef POLCAVOJ_CD_H
#define POLCAVOJ_CD_H

#include "../imp/command.h"

#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

using namespace std;

/**
 * This class is used for change a directory on the server. It's descendant of CCommand.
 *
 * This command represent command "cd" in bash.
 */

class CCd : public CCommand{
    public:
        /**
         * Constructor in which is called constructor of parent and some variables which are used for parsing and argument.
         *
         * @param[in] type Type of mode in which is program currently running.
         * @param[in] path Current path on the server.
         * @param[in] dest Destination directory.
         * @param[in] sock Socket which is used for the connection to the FTP server.
         * @param[in] visible Variable which determines if a response from server should be printed.
         */
        CCd( const string & type, const string & path, const string & dest, int sock, bool visible ) : CCommand(type), m_sock(sock), m_path(path),
                                                                                         m_dest(dest), m_success(false), m_visible(visible){}
        virtual ~CCd(){};

        /**
         * Main method in class. Method controls process of the command and send a command to the server.
         */
        virtual void executeCommand();

        /**
         * Method which print to the command line.
         *
         * @param[in,out] os Ostream which print a current directory on the server.
         */
        virtual void print( ostream & ) const;

        /**
         * Getter of the current path.
         *
         * @return Path to the current folder.
         */
        string actualPath() const;

        /**
         * Method for the communication from the server.
         *
         * Method which receive data from the server and print them (if and m_visible is true) to the command line.
         * Method also set variable m_success if the change was successful.
         */
        void treatResponseCd();

        /**
         * Method which change path ( variable m_path ) to the current path.
         *
         * It's used for the CClient, where is variable m_dir ( path of the current dir ).
         */
        void changePath();

        /**
         *  Method which delete an quotation marks from the argument.
         */
        void treatDest();

        /**
         * Method which solve a problematic argument - "../" "./" "/" etc.
         */
        void magicString();

    protected:
        /** Variable which represent a connection to the FTP server. */
        int m_sock;

        /** Path to the current directory. */
        string m_path;

        /** Variable represent destination directory. */
        string m_dest;

        /** Buffer for received data. */
        char m_buffer[ MAX_BUFFER_LENGTH + 1 ];

        /** Variable which represent success of command. */
        bool m_success;

        /** Variable which determines if the response from the server will be printed. */
        bool m_visible;

};

#endif //POLCAVOJ_CD_H
