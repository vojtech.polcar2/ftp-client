#ifndef POLCAVOJ_DIREC_H
#define POLCAVOJ_DIREC_H

#include "../imp/command.h"
#include "../com/cd.h"
#include "../com/list.h"
#include "../com/get.h"

#include <string>
#include <iostream>
#include <map>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

/* Creating a directories */
#include <sys/stat.h>

/**
 *  This class is used for download/deleting a folders from the server. It's descendant of CCommand.
 *
 *  Folders are downloaded in to the folder downloads/. In default setting an object is asking on every file/folder, if it should be downloaded.
 */
class CDirec : public CCommand{
    public:
        /**
         * Constructor in which is called constructor of parent and some variables, which are need for communication with server.
         *
         * @param[in] type Type of mode in which is program currently running.
         * @param[in] path Path on the local PC - downloads/path.
         * @param[in] dir Name of the directory which should be downloaded.
         * @param[in] sock Socket used for the communication.
         * @param[in] address Address of the server.
         * @param[in] asking The variable that determines whether the object will inquire whether to download.
         * @param[in] mode Mode which determines if a folder will be deleted or downloaded.
         */
        CDirec( const string & type, const string & path, const string & dir, int sock, const string & address, bool asking, bool mode) : CCommand( type ), m_path(path),
                                                                           m_actDir(dir), m_sock(sock), m_pasvport(0), m_address(address),
                                                                           m_pasvrec(""), m_status(true), m_asking(asking), m_mode(mode) {}
        virtual ~CDirec(){}

        /**
         * Method which control and calls other methods.
         *
         * First it change directory, then use command ls for download a structure of the folder and in the end calls the right method depending on m_mode.
         */
        virtual void executeCommand();

        /**
         * The method in this class is unused.
         *
         * @param[in,out] os Ostream which doesnt do anything.
         */
        virtual void print( ostream & os ) const{ os << endl;}

        /**
         * Getter which determine success of download or delete directory.
         *
         * @return Method return variable m_status.
         */
        bool getStatus() const;

        /**
         * Method which just receive data from the server and throw them away.
         */
        void treatResponse();

        /**
         * Method which parse a line after a command PASV.
         * Method set a variable m_address.
         *
         * @return Port of the new data socket.
         */
        int calcPASV();

        /**
         * Method which receive a line from the server and call a method calcPASV.
         */
        void treatPASV();

        /**
         * Method which clear a variables m_pasvport, m_pasvrec, m_address for next usage.
         */
        void clearDataSocket();

        /**
         * Method which take and parse an answer from the user after question on download.
         *
         * @param[in] line Line from the user.
         *
         * @return 1 if answer is yes, 0 if answer is no and -1 if answer is invalid. In that case is question asked again.
         */
        int treatAnswer( const string & line ) const;

        /**
         * Method which download everything in the current directory.
         *
         * In case of a file it calls class CGet and download it.
         * In case of a folder it calls a new class of CDirec.
         */
        void downloadDirectory();

        /**
         * Method which remove everything in the current directory.
         *
         * In case of a file it send to the server a simple command DELETE.
         * In case of a folder it calls a new class of CDirec.
         */
        void removeDirectory();

    protected:
        /** Path on the local PC. */
        string m_path;

        /** Path on the server. */
        string m_actDir;

        /** Socket for the communication with a server. */
        int m_sock;

        /** Port for the data socket. */
        int m_pasvport;

        /** Address for the data socket. */
        string m_address;

        /** Line which is received after the command PASV. */
        string m_pasvrec;

        /** Map of the structure of the folder. */
        map< string , char > dirContent;

        /** The variable which represent success of the command. */
        bool m_status;

        /** The variable that determines whether the object will inquire whether to download. */
        bool m_asking;

        /** Mode of the object - true for download, false for delete. */
        bool m_mode;

        /** Buffer for the data from server. */
        char m_buffer[ MAX_BUFFER_LENGTH + 1 ];

};

#endif //POLCAVOJ_DIREC_H
