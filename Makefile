PROGRAM=polcavoj

CC=g++
CFLAGS=-std=c++11 -Wall -pedantic -Wextra -g -Wno-long-long -O0 -ggdb

all: compile doc

run: compile
	./$(PROGRAM) polcavoj.wz.cz

compile: $(PROGRAM) downloads/

doc: src/*
	doxygen Doxyfile

$(PROGRAM): objs/main.o objs/client.o objs/factory.o objs/program.o objs/command.o objs/help.o objs/cd.o objs/list.o objs/get.o objs/direc.o objs/send.o
	$(CC) $(CFLAGS) $^ -o $@

objs/main.o: src/main.cpp src/imp/program.h src/imp/program.cpp | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/program.o: src/imp/program.cpp src/imp/program.h src/pom/factory.h src/imp/command.h src/com/help.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/client.o: src/imp/client.cpp src/imp/client.h src/pom/constants.h src/imp/command.h src/com/help.h src/com/cd.h src/com/list.h src/com/get.h src/com/direc.h src/com/send.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/factory.o: src/pom/factory.cpp src/pom/factory.h src/imp/client.h src/imp/program.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/command.o: src/imp/command.cpp src/imp/command.h src/imp/client.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/help.o: src/com/help.cpp src/com/help.h src/imp/command.h | objs
	$(CC) $(CFLAGS) -c $< -o $@	

objs/cd.o: src/com/cd.cpp src/com/cd.h src/imp/command.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/list.o: src/com/list.cpp src/com/list.h src/imp/command.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/get.o: src/com/get.cpp src/com/get.h src/imp/command.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/direc.o: src/com/direc.cpp src/com/direc.h src/imp/command.h src/com/list.h src/com/cd.h src/com/get.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/send.o: src/com/send.cpp src/com/send.h src/imp/command.h | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/:
	mkdir objs

downloads/:
	mkdir downloads

clean:
	rm -rf $(PROGRAM) objs/ doc/ downloads/ 2>/dev/null
