#ifndef POLCAVOJ_CONSTANTS_H
#define POLCAVOJ_CONSTANTS_H

/** Name of the program in parameters. */
const int PARAMETER_PROG = 0;
/** Name of the server in parameters. */
const int PARAMETER_SERVER = 1;

/** Return value when application ended successfully. */
const int RETURN_OK = 0;
/** Return value when application have too many arguments. */
const int RETURN_ERROR_ARGUMENTS = 1;
/** Return value when connecting to FTP server failed. */
const int RETURN_ERROR_CREATE = 2;

/** Command for exit application.*/
const char * const COMMAND_END = "exit";
/** Command for display all commands.*/
const char * const COMMAND_HELP = "help";
/** Command for connect to server. */
const char * const COMMAND_CONNECT = "open";
/** Command for quit the FTP server. */
const char * const COMMAND_CLOSE = "close";
/** Command for make a directory. */
const char * const COMMAND_MKDIR = "mkdir";
/** Command for browsing on the server. */
const char * const COMMAND_CD = "cd";
/** Command for list of files in directory.*/
const char * const COMMAND_LS = "ls";

/** Server - Authentication command USER. */
const char * const SRV_USER = "USER ";
/** Server - Authentication command PASS. */
const char * const SRV_PASS = "PASS ";
/** Server - command for quit. */
const char * const SRV_QUIT = "QUIT\r\n";


/** Response from server - login failed */
const char * const LOGIN_FAILED = "530 Login incorrect.\r";
/** Command HELP which display all available commands. */
const char * const HELP = "MODE \"ftp>\" - program is waiting for connection\n"
                          "open <server_address> - Open connection to FTP server.\n"
                          "exit - Exit FTP client.\n"
                          "help - Display all available commands.\n"
                          "MODE \"ftp->\" - program is connected to the FTP server\n"
                          "close - Close connection to FTP server\n";

/** Size of the buffer. */
const int MAX_BUFFER_LENGTH = 256;
/** Size of command length. */
const int MAX_COMMAND_LNT = 50;
/** Port number */
const char * const PORT = "21";


#endif //POLCAVOJ_CONSTANTS_H
