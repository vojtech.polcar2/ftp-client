#include "client.h"

CClient::~CClient( void ){
    close( m_sock );
}

bool CClient::isRunning( void ){
    if( ! m_running ) return false;
    return true;
}

string CClient::parseArg( const string & line, int & pos) const{
    string res = "";
    bool flag = false;
    for( size_t i = pos; i < line.length(); ++i ) {
        pos++;
        if( res == "" && ! flag && line[i] == '"' ) {
            flag = true;
            continue;
        }
        else if ( flag && line[i] == '"' ){
            if( i != line.length() - 1 &&  line[i + 1] != ' ' && line[i + 1] != '\t' )
                return "";
            else
                return res;
        }

        if ( res != "" && flag && (line[i] == ' ' || line[i] == 't') ) {
            res += line[i];
            continue;
        }

        else if( res != "" && flag && i == line.length() - 1 ) {
            return "";
        }
        else if ( res != "" && ! flag && (line[i] == ' ' || line[i] == '\t') ){
            break;
        }

        if(  line[i] == ' ' || line[i] == '\t' )
            continue;
        else
            res += line[i];
    }

    return res;
}

string CClient::parseLine( const string & line, int & pos ) const{
    string res = "";
    for( size_t i = pos; i < line.length(); ++i ){
        if( res != "" && ( line[i] == ' ' || line[i] == '\t' ) ) {
            pos = i;
            break;
        }

        if(  line[i] == ' ' || line[i] == '\t' )
            continue;
        else
            res += line[i];
    }
    if( res.length() != 0 && pos == 0 )
        pos += line.length();

    return res;
}

void CClient::clearDataSocket(){
    m_address = "";
    m_pasvrec = "";
    m_pasvport = 0;
}

void CClient::commandCD( const string & line, int pos ){
    string argument = "";
    if( ! pos )
        cout << ERROR_CD << endl;

    else{
        argument = parseArg( line, pos );

        /* space in name */
        if( argument[0] == '"' && argument.length() != 0 )
            changeArg( argument );

        if( argument == "" ) {
            cout << ERROR_CD << endl;
            return;
        }

        CCd cwd ( TYPE_CLIENT, m_dir, argument, m_sock, true );
        cwd.executeCommand();
        if( cwd.actualPath() != m_dir )
            dirContent.clear();

        m_dir = cwd.actualPath();
    }
}

void CClient::commandLS( const string & line, int pos ){
    string argument = "", toServer = "";
    bool signal = true;

    send( m_sock, SRV_PASV, 6, 0 );
    treatPASV();
    if( pos == 0 )
        pos = 2;

    argument = parseArg( line, pos );

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    makePath( argument );
    argument == "" ? toServer = SRV_LIST + m_dir + "\r\n" : toServer = SRV_LIST + argument + "\r\n";
    if ( argument == "" || argument == "/." || argument == "/./" || argument == "//" )
        dirContent.clear();
    else
        signal = false;

    send( m_sock, toServer.c_str(), toServer.length(), 0 );
    CList ls( TYPE_CLIENT, m_pasvport, m_address, signal, true );
    if( ls.connectToServer() )
        return;

    treatResponse();
    ls.executeCommand();
    treatResponse();

    if( argument == "" )
        dirContent = ls.getDirContent();

    clearDataSocket();
}

void CClient::commandGET( const string & line, int pos ){
    string argument = "", toServer = "";
    argument = parseArg(line, pos);
    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    if (argument == "") {
        cerr << ERROR_GET << endl;
        return;
    }

    auto it = dirContent.find(argument);
    if ( it == dirContent.end() ){
        cerr << "No such file." << endl;
        return;
    }

    if( it->second == 'd' ) {
        cerr << argument << " is a directory." << endl;
        return;
    }

    send(m_sock, SRV_PASV, 6, 0);
    treatPASV();

    toServer = SRV_GET + argument + "\r\n";
    send( m_sock, toServer.c_str(), toServer.length(), 0 );
    CGet get( TYPE_CLIENT, m_pasvport, m_address, argument );
    if( get.connectToServer() )
        return;

    treatResponse();
    get.executeCommand();
    treatResponse();

    clearDataSocket();
}

void CClient::commandGETD( const string & line, int pos ) const{
    bool asking = true;
    string argument = "", toServer = "";

    argument = parseArg(line, pos);
    if( argument == "-A" ){
        asking = false;
        argument = "";
        argument = parseArg( line, pos );
    }

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    if ( argument == "" ) {
        cerr << ERROR_GETD << endl;
        return;
    }

    auto it = dirContent.find(argument);
    if ( it == dirContent.end() ){
        cerr << "No such directory." << endl;
        return;
    }

    if( it->second == 'f' ){
        cerr << argument << " is a file." << endl;
        return;
    }

    string path = DOWNL_DIR + argument;
    mkdir( path.c_str(), 0744);

    string source = "";

    m_dir == "/" ? source = m_dir + argument : source = m_dir + '/' + argument;

    CDirec dir( TYPE_CLIENT, argument, source, m_sock, m_address, asking, true );
    dir.executeCommand();

    if( dir.getStatus() )
        cout << "Download was successful." << endl;
    else
        cout << "Error during download." << endl;

    CCd cwd ( TYPE_CLIENT, m_dir, m_dir, m_sock, false );
    cwd.executeCommand();
}

void CClient::commandMKDIR( const string & line, int pos ){
    string argument = parseArg( line, pos ), toServer = "";

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    if ( argument == "" ) {
        cerr << ERROR_MKDIR << endl;
        return;
    }

    toServer = SRV_MKDIR + argument + "\r\n";
    send( m_sock, toServer.c_str(), toServer.length(), 0 );
    treatResponse();
    dirContent.clear();
}

void CClient::commandRMDIR( const string & line, int pos ){
    bool del = false;
    string argument = parseArg( line, pos ), toServer = "";
    if( argument == "-R" ){
        argument = "";
        argument = parseArg( line, pos );
        del = true;
    }

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    if ( argument == "" ) {
        cerr << ERROR_RMDIR << endl;
        return;
    }

    if( del ){
        string source = "";

        m_dir == "/" ? source = m_dir + argument : source = m_dir + '/' + argument;

        CDirec dir( TYPE_CLIENT, argument, source, m_sock, m_address, false, false );
        dir.executeCommand();

        CCd cwd ( TYPE_CLIENT, m_dir, m_dir, m_sock, false );
        cwd.executeCommand();

    }

    toServer = SRV_RMDIR + argument + "\r\n";
    send(m_sock, toServer.c_str(), toServer.length(), 0);
    treatResponse();
    dirContent.clear();
}

void CClient::commandDELETE( const string & line, int pos ){
    string argument = parseArg( line, pos ), toServer = "";

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0)
        changeArg( argument );

    if ( argument == "" ) {
        cerr << ERROR_DELETE << endl;
        return;
    }

    toServer = SRV_DEL + argument + "\r\n";
    send( m_sock, toServer.c_str(), toServer.length(), 0 );
    treatResponse();
    dirContent.clear();
}

void CClient::commandSEND( const string & line, int pos ){
    string argument = "", toServer = "", name = "";
    argument = parseArg(line, pos);

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    if (argument == "") {
        cerr << ERROR_SEND << endl;
        return;
    }

    name = parseArg( line, pos );
    if( name[0] == '"' && name.length() != 0 )
        changeArg( name );

    if ( name == "" ) {
        cerr << ERROR_SEND << endl;
        return;
    }

    send(m_sock, SRV_PASV, 6, 0);
    treatPASV();

    toServer = SRV_SEND + name + "\r\n";
    send( m_sock, toServer.c_str(), toServer.length(), 0 );

    CSend post( TYPE_CLIENT, m_pasvport, m_address, argument, 0 );
    if( post.connectToServer() )
        return;

    treatResponse();
    post.executeCommand();
    treatResponse();

    clearDataSocket();

    if( ! post.getError() )
        return;

    toServer = SRV_DEL + name + "\r\n";
    send( m_sock, toServer.c_str(), toServer.length(), 0 );
    treatResponse();
}

void CClient::commandCREATE(const string &line, int pos){
    string argument = "", toServer = "";
    argument = parseArg(line, pos);

    /* space in name */
    if( argument[0] == '"' && argument.length() != 0 )
        changeArg( argument );

    if (argument == "") {
        cerr << ERROR_CREATE<< endl;
        return;
    }

    for( size_t i = 0; i < argument.length(); ++i ) {
        if (argument[i] == '/') {
            cerr << ERROR_CREATE << endl;
            return;
        }
    }

    send(m_sock, SRV_PASV, 6, 0);
    treatPASV();

    toServer = SRV_SEND + argument + "\r\n";
    send( m_sock, toServer.c_str(), toServer.length(), 0 );

    CSend create( TYPE_CLIENT, m_pasvport, m_address, argument, 1 );
    if( create.connectToServer() )
        return;

    treatResponse();
    create.executeCommand();
    treatResponse();

    clearDataSocket();
}

int CClient::treatCommand( const string & line ){
    string command = "", argument = "", toServer = "";
    int pos = 0, actInvalid = m_invalidCnt;
    command = parseLine( line, pos );

    /* COMMAND CLOSE */
    if( command == COMMAND_CLOSE ){
        send( m_sock, SRV_QUIT, 6, 0);
        treatResponse();
        return -1;
    }

    /* COMMAND HELP */
    else if( command == COMMAND_HELP ){
        CHelp help( TYPE_CLIENT );
        help.executeCommand();
    }

    /* COMMAND CD*/
    else if( command == COMMAND_CD ) {
        commandCD( line, pos );
    }

    /* COMMAND LS */
    else if( command == COMMAND_LS ) {
        if ( ! m_passive ){
            cout << PASV_WARN << endl;
            return 0;
        }

        commandLS( line, pos );
    }

    /* COMMAND PASV*/
    else if( command == COMMAND_PASV ){
        ! m_passive ? m_passive = true : m_passive = false;
        m_passive ? cout << PASV_ON << endl : cout << PASV_OFF << endl;
    }

    /* COMMAND GET*/
    else if( command == COMMAND_GET ) {
        if (!m_passive)
            cout << PASV_WARN << endl;

        commandGET( line, pos );
    }

    /* COMMAND GETD */
    else if( command == COMMAND_GETD ){
        if (!m_passive)
            cout << PASV_WARN << endl;

        commandGETD( line, pos );
    }

    /* COMMAND MKDIR */
    else if( command == COMMAND_MKDIR )
        commandMKDIR( line, pos );


    /* COMMAND RMDIR */
    else if( command == COMMAND_RMDIR ) {
        commandRMDIR( line, pos );
    }

    /* COMMAND DELETE */
    else if( command == COMMAND_DELETE ) {
        commandDELETE( line, pos );
    }

    /* COMMAND CREATE */
    else if( command == COMMAND_CREATE ){
        if (!m_passive)
            cout << PASV_WARN << endl;

        commandCREATE( line, pos );
    }

    /* COMMAND SEND */
    else if( command == COMMAND_SEND ){
        if (!m_passive)
            cout << PASV_WARN << endl;

        commandSEND( line, pos );
    }


    /* COMMAND IN WRONG MODE */
    else if( command == COMMAND_OPEN )
        cout << ERROR_CLIENT << endl;

    /* COMMAND EXIT */
    else if ( command == COMMAND_END )
        cout << ERROR_FIRST_CLOSE << endl;

    /* EMPTY LINE */
    else if( command == "" )
        return 0;

    /* INVALID COMMAND */
    else {
        cout << INVALID_COMMAND << endl;
        m_invalidCnt++;
    }

    /* Robot checker */
    if( m_invalidCnt == 10 ){
        cout << ERROR_STUPIDITY << endl;
        return -2;
    }

    if( m_invalidCnt == actInvalid )
        m_invalidCnt = 0;

    return 0;
}

void CClient::changeArg(string & argument) const{
    string pom = "";
    for (size_t i = 1; i < argument.length() - 1; ++i)
        pom += argument[i];

    argument = pom;
}

void CClient::makePath( string & arg) const{
    string res = "";
    if( arg == "" )
        return;

    if( arg[0] != '/' )
        arg = m_dir + '/' + arg;

    if( ( arg[0] == '.' && arg.length() == 1 ) || ( arg.length() == 2 && arg[0] == '.' && arg[1] == '/' ) ){
        arg = "";
        return;
    }

    if( ( arg.length() > 2 && arg[0] == '.' && arg[1] == '/' ) ) {
        res = m_dir;
        for ( size_t i = 2; i < arg.length(); ++i )
            res += arg[i];

        arg = res;
        return;
    }
}

int CClient::calcPASV(){
    bool flag = true;
    string number1 = "", number2 = "", address = "";
    int symbols = 4, num1 = 0, num2 = 0;
    for( size_t i = 0; i < m_pasvrec.length(); ++i ){
        if( flag && m_pasvrec[i] != '(')
            continue;
        else
            flag = false;

        if( m_pasvrec[i] == ')' )
            break;

        if( m_pasvrec[i] == ',' ) {
            symbols--;
            if( symbols > 0 )
                address += '.';
            continue;
        }

        if( symbols == 0)
            number1 += m_pasvrec[i];

        else if( symbols == -1 )
            number2 += m_pasvrec[i];
        else if( symbols > 0 && m_pasvrec[i] != '(' && m_pasvrec[i] != ',' )
            address += m_pasvrec[i];

    }

    num1 = atoi(number1.c_str() );
    num2 = atoi(number2.c_str() );

    m_address = address;

    return ( (num1 * 256) + num2 );
}

void CClient::treatPASV(){
    bool flag = false;
    int cntBytes;
    char previousByte, actualByte;
    string text = "";
    m_pasvrec = "";
    while (1) {
        cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0);

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                cout << text << endl;
                m_pasvrec += text;
                text = "";
                flag = true;
            }
            else
                text += m_buffer[i];

            previousByte = actualByte;
        }

        if ( flag ) {
            flag = false;
            break;
        }
    }
    m_pasvport = calcPASV();
}

void CClient::treatResponse(){
    bool flag = false;
    int cntBytes;
    char previousByte, actualByte;
    string text = "", pom = "";
    while (1) {
        cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0);

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                cout << text << endl;
                pom += text;
                text = "";
                flag = true;
            }
            else
                text += m_buffer[i];

            previousByte = actualByte;
        }

        if ( flag ) {
            flag = false;
            break;
        }
    }

    if( pom == LOGIN_FAILED  )
        m_running = false;
}

bool CClient::authenticate(){
    int status = 0;
    string command = "", message = "";
    while( 1 ) {
        treatResponse();
        if( ! m_running ){
            send( m_sock, SRV_QUIT, 6, 0);
            treatResponse();
            return false;
        }
        status++;

        if (status == 1) {
            cout << "User: ";
            cin >> command;
            message += SRV_USER + command + "\r\n";
            send(m_sock, message.c_str(), message.length(), 0);
        }
        else if (status == 2){
            cout << "Password: ";
            cin >> command;
            message += SRV_PASS + command + "\r\n";
            send( m_sock, message.c_str(), message.length(), 0);
        }
        else if( status == 3 )
            return true;

        message = "";
    }
}

int CClient::communicate(){
    string command = "";
    int status = 0;

    fd_set rd;
    FD_ZERO ( &rd );
    FD_SET ( m_sock, &rd );

    if( ! authenticate() ){
        getline( cin, command );
        return 0;
    }

    getline( cin, command );
    while( 1 ){
        cout << "ftp->";
        getline( cin, command );
        status = treatCommand( command );
        if( status == -1 || ! m_running )
            return 0;
        else if ( status == -2 )
            return -1;
    }
}