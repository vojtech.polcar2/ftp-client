#include "list.h"

CList::~CList(){
    close(m_sock);
}

map< string, char > CList::getDirContent() const{
    return dirContent;
}

void CList::parseLine( const string & line ){
    char type;
    bool inBlock = true;
    string name = "";
    int blocks = 8;

    line[0] == 'd' ? type = 'd' : type = 'f';

    for( size_t i = 0; i < line.length() - 1 ; ++i ) {
        if ( inBlock && blocks == 0 ){
            name += line[i];
            continue;
        }

        if( inBlock && ( line[i] == ' ' || line[i] == '\t' ) ){
            blocks--;
            inBlock = false;
        }

        if( line[i] == ' ' || line[i] == '\t' )
            continue;
        else if( ! inBlock ){
            inBlock = true;
            if( blocks == 0 )
                name += line[i];
        }
    }

    if( name != "." && name != ".." )
        dirContent[name] = type;

}

void CList::executeCommand(){
    treatResponse();
    if( m_visible )
        cout << *this;

    send( m_sock, SRV_QUIT, 6, 0);
}

void CList::treatResponse(){
    int cntBytes;
    char previousByte, actualByte;
    char m_buffer[ MAX_BUFFER_LENGTH + 1 ];
    string text = "", line = "";
    while (1) {
        if( ( cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0) ) <= 0 )
            return;


        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                /* Download or display */
                if( m_visible )
                    cout << text << endl;
                text = "";
            }
            else {
                text += m_buffer[i];
                line += m_buffer[i];
            }

            if( actualByte == '\n'  && m_signal ) {
                parseLine( line );
                line = "";
            }

            previousByte = actualByte;
        }
    }
}

void CList::print( ostream & os ) const{
    os << "Loaded " << dirContent.size() << " items." << endl;
}

int CList::connectToServer(){
    struct addrinfo * ai;

    string port = to_string(m_port);

    if ( getaddrinfo( m_server.c_str(), port.c_str(), NULL, &ai ) != 0 ) {
        cerr << "Error during getaddrinfo()." << endl;
        return 1;
    }

    m_sock = socket ( ai -> ai_family, SOCK_STREAM, 0 );
    if ( m_sock == -1 ){
        freeaddrinfo ( ai );
        cerr << "Error during socket()." << endl;
        return 1;
    }

    if ( connect ( m_sock, ai -> ai_addr, ai -> ai_addrlen ) != 0 ){
        close ( m_sock );
        freeaddrinfo ( ai );
        cerr << "Error during connect()." << endl;
        return 1;
    }

    freeaddrinfo ( ai );
    return 0;
}