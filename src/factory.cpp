#include "factory.h"

CClient * CFactory::createClient ( const string address ){
    struct addrinfo * ai;

    if (getaddrinfo( address.c_str(), PORT, NULL, &ai ) != 0 ) {
        cerr << "getaddrinfo" << endl;
        return NULL;
    }

    int sock = socket ( ai -> ai_family, SOCK_STREAM, 0 );
    if ( sock == -1 ){
        freeaddrinfo ( ai );
        cerr << "socket" << endl;
        return NULL;
    }

    if ( connect ( sock, ai -> ai_addr, ai -> ai_addrlen ) != 0 ){

        close ( sock );
        freeaddrinfo ( ai );
        cerr << "connect" << endl;
        return NULL;
    }

    freeaddrinfo ( ai );
    CClient * client = new CClient( sock );
    cout << "Connected to " << address << endl;
    return client;
}


void CFactory::deleteClient( CClient * & client ){
    delete client;
    client = NULL;
}