#include "cd.h"

void CCd::treatResponseCd(){
    bool flag = false;
    int cntBytes;
    char previousByte, actualByte;
    string text = "", pom = "", error = "550 " + m_dest + ": No such file or directory\r";
    while (1) {
        cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0);

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                if( m_visible )
                    cout << text << endl;
                pom += text;
                text = "";
                flag = true;
            }
            else
                text += m_buffer[i];

            previousByte = actualByte;
        }

        if ( flag ) {
            flag = false;
            break;
        }
    }

    pom == error ? m_success = false : m_success = true;
}

void CCd::magicString(){
    int branches = 0, symbols = 0;
    string res = "/", pom = "";
    for( size_t i = 0;i < m_dest.length(); i += 3 ){
        if( m_dest[i] == '.' && m_dest[i + 1] == '.' && m_dest[i + 2] == '/' )
            branches++;
        else break;
    }

    for( size_t i = 0; i < m_path.length(); ++i )
        if( m_path[i] == '/') symbols++;

    if( branches >= symbols ){
        for( size_t i = ( branches * 3 ); i < m_dest.length(); ++i )
            res += m_dest[i];

        m_path = res;
        return;
    }

    symbols -= branches;

    for( size_t i = 0; i < m_path.length(); ++i ){
        if( symbols == 0 && m_path[i] == '/') break;
        if( m_path[i] == '/') symbols--;
        pom += m_path[i];
    }

    for( size_t i = (branches * 3); i < m_dest.length(); ++i )
        pom += m_dest[i];

    m_path = pom;
}

void CCd::changePath(){
    string pom = "";

    /* throw away './' */
    if( m_dest.length() > 2 && m_dest[0] == '.' && m_dest[1] == '/'){
        for( size_t i = 2; i < m_dest.length(); ++i )
            pom += m_dest[i];

        m_dest = pom;
    }

    if( m_dest[0] == '/' ){
        m_path = m_dest;
        return;
    }
    else if( (m_dest.length() == 1 && m_dest[0] == '.') || ( m_dest.length() == 1 && m_dest[0] == '.' && m_dest[1] == '/' ) )
        return;
    else if( m_dest[0] != '/' && m_dest[0] != '.' )
        m_path != "/" ? m_path += "/" + m_dest : m_path += m_dest;
    else if( m_dest.length() > 1 && m_dest[0] == '.' && m_dest[1] == '.' ){
        magicString();
    }
}

void CCd::treatDest(){
    string pom = "";
    for( size_t i = 1; i < m_dest.length() - 1; ++i )
        pom += m_dest[i];

    m_dest = pom;
}

void CCd::executeCommand(){
    if( m_dest[0] == '"') treatDest();
    if( m_dest == ".." ) m_dest = "../";
    string command = SRV_CWD + m_dest + "\r\n";
    send( m_sock, command.c_str(), command.length(), 0);
    treatResponseCd();
    if( m_success )
        changePath();

    cout << *this;
}

void CCd::print( ostream & os ) const{
    if( m_visible )
        os << "Actual directory: " << m_path << endl;
}

string CCd::actualPath() const{
    return m_path;
}