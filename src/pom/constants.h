#ifndef POLCAVOJ_CONSTANTS_H
#define POLCAVOJ_CONSTANTS_H

/** Name of the program in parameters. */
const int PARAMETER_PROG = 0;
/** Name of the server in parameters. */
const int PARAMETER_SERVER = 1;

/** Return value when application ended successfully. */
const int RETURN_OK = 0;
/** Return value when application have too many arguments. */
const int RETURN_ERROR_ARGUMENTS = 1;

/** Command for exit application.*/
const char * const COMMAND_END = "exit";
/** Command for display all commands.*/
const char * const COMMAND_HELP = "help";
/** Command for connect to server. */
const char * const COMMAND_OPEN = "open";
/** Command for quit the FTP server. */
const char * const COMMAND_CLOSE = "close";
/** Command for make a directory. */
const char * const COMMAND_MKDIR = "mkdir";
/** Command for delete a file. */
const char * const COMMAND_DELETE = "delete";
/** Command for delete a directory. */
const char * const COMMAND_RMDIR = "rmdir";
/** Command for browsing on the server. */
const char * const COMMAND_CD = "cd";
/** Command for list of files in directory.*/
const char * const COMMAND_LS = "ls";
/** Command for enable/disable passive mode.*/
const char * const COMMAND_PASV = "pasv";
/** Command for download file from FTP Server. */
const char * const COMMAND_GET = "get";
/** Command for download directory from FTP server. */
const char * const COMMAND_GETD = "getd";
/** Command fot upload a file. */
const char * const COMMAND_SEND = "send";
/** Command for create an empty file. */
const char * const COMMAND_CREATE = "create";

/** Server - Authentication command USER. */
const char * const SRV_USER = "USER ";
/** Server - Authentication command PASS. */
const char * const SRV_PASS = "PASS ";
/** Server - List command. */
const char * const SRV_LIST = "LIST ";
/** Server - Download a file command. */
const char * const SRV_GET = "RETR ";
/** Server - Rmdir command. */
const char * const SRV_RMDIR = "RMD ";
/** Server - Mkdir command. */
const char * const SRV_MKDIR = "MKD ";
/** Server - Delete a file command. */
const char * const SRV_DEL = "DELE ";
/** Server - Passive mode command PASV. */
const char * const SRV_PASV = "PASV\r\n";
/** Server - Change directory command CWD. */
const char * const SRV_CWD = "CWD ";
/** Server - command for quit. */
const char * const SRV_QUIT = "QUIT\r\n";
/** Server - command for send. */
const char * const SRV_SEND = "STOR ";


/** Response from server - login failed */
const char * const LOGIN_FAILED = "530 Login incorrect.\r";
/** Passive warning. */
const char * const PASV_WARN = "Warning: You're not in the passive mode. Please use command pasv.";
/** Passive mode on. */
const char * const PASV_ON = "Passive mode on.";
/** Passive mode off. */
const char * const PASV_OFF = "Passive mode off.";

/** Invalid command. */
const char * const INVALID_COMMAND = "? Invalid command.";
/** Bad syntax after cd. */
const char * const ERROR_CD = "Wrong syntax: Usage: cd <path> or cd / or cd \"<path>\"";
/** Bad syntax after get. */
const char * const ERROR_GET = "Wrong syntax: Usage: get <file>.";
/** Bad syntax after getd. */
const char * const ERROR_GETD = "Wrong syntax: Usage: getd <dir> or getd -A <dir>.";
/** Bad syntax after rmdir. */
const char * const ERROR_RMDIR = "Wrong syntax: Usage: rmdir <dir> or rmdir -R <dir>.";
/** Bad syntax after mkdir. */
const char * const ERROR_MKDIR = "Wrong syntax: Usage: mkdir <dir>.";
/** Bad syntax after delete. */
const char * const ERROR_DELETE = "Wrong syntax: Usage: delete <name>.";
/** Bad syntax after create. */
const char * const ERROR_CREATE = "Wrong syntax: Usage: create <name>.";
/** Bad syntax after send. */
const char * const ERROR_SEND = "Wrong syntax: Usage: send <name> or send <path/name>.";
/** Command in wrong mode - Client. */
const char * const ERROR_CLIENT = "Command is called in wrong mode. You're in client mode.";
/** Command in wrong mode - Program. */
const char * const ERROR_PROGRAM = "Command is called in wrong mode. You're in program mode.";
/** Command exit in mode client. */
const char * const ERROR_FIRST_CLOSE = "First you have to terminate the connection. (command close)";
/** Error when a file doesnt exist during upload. */
const char * const ERROR_EXIST = "File doesnt exist.";
/** Error when a path doesnt lead to the file or folder. */
const char * const ERROR_FILE_FOLDER = " isn't file or folder.";
/** Error when a file is a folder during upload. */
const char * const ERROR_FOLDER = " is a folder.";
/** Message when Invalid command is called 10x. */
const char * const ERROR_STUPIDITY = "You look like a robot, you do not deserve to use this program. Next time use <help>. Bye.";

/** Size of the buffer. */
const int MAX_BUFFER_LENGTH = 1024;
/** Size of command length. */
const int MAX_COMMAND_LNT = 50;
/** Port number */
const char * const PORT = "21";
/** Type 1 from which are called commands. */
const char * const TYPE_PROG = "program";
/** Type 2 from which are called commands. */
const char * const TYPE_CLIENT = "client";
/** Default directory for download. */
const char * const DOWNL_DIR = "downloads/";

#endif //POLCAVOJ_CONSTANTS_H
