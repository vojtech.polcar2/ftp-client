#ifndef POLCAVOJ_SEND_H
#define POLCAVOJ_SEND_H

#include "../imp/command.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/stat.h>

/**
 * This class represents command send. It's descendant of CCommand.
 *
 * Class upload a file on the FTP server. It's used for creating an empty file or uploading a file.
 */

class CSend : public CCommand{
    public:

        /**
         * Constructor in which is called constructor of parent and some variables, which are need for communication with server.
         *
         * @param[in] type Type of mode in which is program currently running.
         * @param[in] port Port of the data socket.
         * @param[in] server Address of the data socket.
         * @param[in] fname Name of the file, which will be create on the server.
         * @param[in] mode Determine if a file should be created or just uploaded.
         */
        CSend( const string & type, int port, const string & server, const string & fname, bool mode ) : CCommand(type), m_port(port), m_sock(0),
                                                                                             m_size(0), m_server(server),
                                                                                             m_fname(fname), m_mode(mode), m_error(false){}
        ~CSend(){};

        /**
         * Method which calls a function depending on mode.
         *
         * Method calls function emptyFile or sendFile and then calls print and terminate the connection.
         */
        virtual void executeCommand();

        /**
         * Method which print a size of a file which has been sent.
         *
         * @param[in,out] os Ostream which is printed to the command line.
         */
        virtual void print( ostream & os ) const;

        /**
        *  Method which will connect class with an FTP server on data socket. From this socket are downloaded all data.
        *
        *  @return In case of success method returns zero, otherwise returns 1. Socket is saved in the local variable m_sock.
        */
        int connectToServer();

        /**
         * Method which creates an empty file in /tmp and send that file on the server.
         */
        void emptyFile();

        /**
         * Method which open an file and sent it on the server.
         */
        void sendFile();

        /**
         * Getter which returns variable m_error (if file doesnt exist or we doesnt have a permissions to read ).
         *
         * @return Variable m_error, depends on method sendFile.
         */
        bool getError() const;

    protected:
        /** Port of the data socket. */
        int m_port;

        /** Socket represents a stream for data. */
        int m_sock;

        /** Size of file which has been sent. */
        int m_size;

        /** Name of the server. */
        string m_server;

        /** Name of the file on the local PC. */
        string m_fname;

        /** Mode of the object. Create and send or open and send the file. */
        bool m_mode;

        /** Variable which is set to true if a file doesnt exist or we doesnt have a permissions to read. */
        bool m_error;

};

#endif //POLCAVOJ_SEND_H
