#include "imp/program.h"

int main(int argc, char * argv[] ) {
    string server = "";
    /**
     * @mainpage FTP Client
     *
     * This semestr project is about simple text-oriented FTP Client.
     * Client could be launched without or with argument ( address of FTP server ). If it's launched with address
     * client will automatically connect to the FTP server.
     *
     * In this application are two modes.\n
     * <B>ftp></B> - This means, that program is waiting for opening connection to FTP server. Only available commands
     * are <I>open</I> and <I>exit</I>.\n
     * <B>ftp-></B> - This means, that program is connected to the FTP server and waiting for commands. In this mode
     * are commands <I>open</I> and <I>exit</I> disabled.\n
     *
     *
     * <B>Command list:</B>\n
     * open "server_address" - <I>Open connection to FTP server.</I>\n
     * exit - <I>Exit program.</I>\n
     * help - <I>Display all available commands.</I>\n
     * close - <I>Close connection to FTP server.</I>\n
     * ls "path" - <I>Display list of files in folder or informations about file.</I>\n
     * <I>Just "ls" will display files in current folder and prepared them for download.</I>\n
     * cd "path" - <I>Change working directory.</I>\n
     * pasv - <I>Enable/disable passive mode.</I>\n
     * mkdir "name" - <I>Create a directory. It could be called as "name" or "path/name".</I>\n
     * rmdir "name" - <I>Remove a directory. It could be called as "name" or "path/name".</I>\n
     * <I>-R - argument which will recursively delete folder. Use it when a folder is not empty.</I>\n
     * delete "name" - <I>Delete a file. It could be called as "name" or "path/name".</I>\n
     * <I>These two commands are working only after command ls, which is applied on current folder and only on files in current folder.</I>\n
     * get "name" - <I>It creates a copy of a file from FTP server, to the directory downloads/.</I>\n
     * getd "dirname" - <I>Command for recursive download of a folder. Default setting is that user must confirm or decline every folder.</I>\n
     * <I>-A - argument which turns off confirming to download. Everything is copied.</I>
     * create "name" - <I>Create an empty file on the FTP server.</I>\n
     * send "path/name" "name-on-server" - <I>Upload a file to the FTP server.</I>\n
     *
     *
     * Created by Vojtěch Polcar.
     */

    if( argc > 2 ){
        cerr << "Usage: " << argv[ PARAMETER_PROG ]<< " server_address or " << argv[ PARAMETER_PROG ] << endl;
        return RETURN_ERROR_ARGUMENTS;
    }

    if( argc == 2 )
        server = argv [ PARAMETER_SERVER ];

    CProgram program ( server );
    program.work();

    return RETURN_OK;
}