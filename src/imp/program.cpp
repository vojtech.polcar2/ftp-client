#include "program.h"

CProgram::CProgram( const string & server ){
    if( server == "" ){
        m_client = NULL;
        m_server = "";
    }
    else{
        if( ( m_client = CFactory::createClient( server ) ) == NULL ){
            cerr << "Error while creating a connection. Wrong address." << endl;
            m_client = NULL;
            m_server = "";
        }else
            m_server = server;
    }

    m_invalidCnt = 0;
}

void CProgram::connect( const string & line, int pos ){
    string server = "";
    server = parseLine( line, pos );
    if( server == "" ){
        cerr << "? Missing server address: Usage: open <server_address>." << endl;
        return;
    }

    if( ( m_client = CFactory::createClient( server ) ) == NULL ){
        cerr << "Error while creating a connection. Wrong address." << endl;
        m_client = NULL;
        m_server = "";
    }
    else
        m_server = server;
}

string CProgram::parseLine( const string & line, int & pos ) const{
    string res = "";
    for( size_t i = pos; i < line.length(); ++i){
        if( res != "" && ( line[i] == ' ' || line[i] == '\t' ) ) {
            pos = i;
            break;
        }

        if(  line[i] == ' ' || line[i] == '\t' )
            continue;
        else
            res += line[i];
    }

    /* In case of that open is on the end of the line. */
    if( res.length() != 0 && pos == 0 )
        pos = line.length();

    return res;
}

int CProgram::treatCommand( const string & line ){
    string command = "";
    int pos = 0, actInvalid = m_invalidCnt;
    command = parseLine( line, pos );

    /* COMMAND EXIT */
    if( command == COMMAND_END )
        return -1;

    /* COMMAND HELP */
    else if( command == COMMAND_HELP ){
        CHelp help( TYPE_PROG );
        help.executeCommand();
    }

    /* COMMAND OPEN */
    else if( command == COMMAND_OPEN ){
        connect( line, pos );
    }

    /* COMMANDS IN WRONG MODE */
    else if( command == COMMAND_CD || command == COMMAND_CLOSE || command == COMMAND_LS ||
             command == COMMAND_MKDIR || command == COMMAND_PASV || command == COMMAND_GET ||
             command == COMMAND_DELETE || command == COMMAND_RMDIR || command == COMMAND_GETD ||
             command == COMMAND_CREATE || command == COMMAND_SEND )
        cout << ERROR_PROGRAM << endl;


    else if( command == "" )
        return 0;


    /* INVALID COMMAND */
    else {
        m_invalidCnt++;
        cout << INVALID_COMMAND << endl;
    }

    if( m_invalidCnt == 10 ){
        cout << ERROR_STUPIDITY << endl;
        return -2;
    }

    if( m_invalidCnt == actInvalid )
        m_invalidCnt = 0;

    return 0;
}

void CProgram::work(){
    int signal = 0;
    string command = "";

    while( 1 ){
        if( m_client != NULL && m_client->isRunning()  ){
            signal = m_client->communicate();
            CFactory::deleteClient( m_client );
            if( signal < 0 )
                break;
        }

        cout << "ftp>";
        getline( cin, command );
        signal = treatCommand( command );
        if( signal < 0 )
            break;
    }
}