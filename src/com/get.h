#ifndef POLCAVOJ_GET_H
#define POLCAVOJ_GET_H

#include "../imp/command.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

/**
 * This class is used for download a file from the server. It's descendant of CCommand.
 *
 * Files are downloaded in to the folder downloads/.
 */

class CGet : public CCommand{
    public:
        /**
         * Constructor in which is called constructor of parent and some variables, which are need for communication with server.
         *
         * @param[in] type Type of mode in which is program currently running.
         * @param[in] port Port for data socket.
         * @param[in] server Address of server.
         * @param[in] fname Name of the file on the server.
         */
        CGet( const string & type, int port, const string & server, const string & fname ) : CCommand(type), m_port(port), m_sock(0),
                                                                                             m_size(0), m_server(server), m_fname(fname){}
        /**
         * Destructor which closed the connection to the data socket.
         */
        ~CGet();

        /**
         * Main method in class. Method just controls and calls other methods.
         */
        virtual void executeCommand();

        /**
         *  Method which will connect class with an FTP server on data socket. From this socket are downloaded all data.
         *
         *  @return In case of success method returns zero, otherwise returns 1. Socket is saved in the local variable m_sock.
         */
        int connectToServer();

        /**
         * Method which print to the command line.
         *
         * @param[in,out] os Ostream which print how many bytes was received.
         */
        virtual void print( ostream & os ) const;

        /**
         * Main method for the communication with data socket.
         *
         * Method which receive data from the server and save them to the file.
         */
        void treatResponse();

    protected:
        /** Port of the data socket. */
        int m_port;

        /** Variable which represent a data socket. */
        int m_sock;

        /** Size of the received file. */
        int m_size;

        /** Address of the server. */
        string m_server;

        /** Name of the file. ( on the server and in the downloads/ ) */
        string m_fname;

};

#endif //POLCAVOJ_GET_H
