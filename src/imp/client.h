#ifndef POLCAVOJ_CLIENT_H
#define POLCAVOJ_CLIENT_H

#include "../pom/constants.h"
#include "../com/help.h"
#include "../com/cd.h"
#include "../com/list.h"
#include "../com/get.h"
#include "../com/direc.h"
#include "../com/send.h"

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <map>

/* Creating a directories */
#include <sys/stat.h>

using namespace std;

/**
 * Main class for FTP client. All commands are treat there.
 *
 * Class create an objects which are depends on command. Also, class have an variable which describe a structure of the current folder.
 */

class CClient{
    public:
        /**
         * Constructor, which set all variables to the default setting.
         *
         * @param[in] sock Socket for communication with a server.
         */
        CClient( int sock ) : m_running(true), m_passive(false), m_sock(sock), m_pasvport(0), m_address(""), m_pasvrec(""), m_dir("/"), m_invalidCnt(0){}

        /**
         * Destructor which close a socket to the FTP server.
         */
        ~CClient( void );

        /**
         * Getter which signals if is connection okay and client is running.
         *
         * @return In case of program run returns true, otherwise false.
         */
        bool isRunning( void );

        /**
         * Main method for FTP Client.
         *
         * In this method commands are entered.
         *
         * @return Zero after command close, -1 after 10x INVALID_COMMAND.
         */
        int communicate();

        /**
         * Method which performs an authentication to the server.
         *
         * @return In case of succes true, otherwise false.
         */
        bool authenticate();

        /**
         * Method which receive an answer from the server.
         *
         * Method set a variable m_running in case of login failed.
         */
        void treatResponse();

        /**
         * Method which receive a line from the server and call a method calcPASV.
         */
        void treatPASV();

        /**
         * Method which parse a line after a command PASV.
         * Method set a variable m_address.
         *
         * @return Port of the new data socket.
         */
        int calcPASV();

        /**
         * This method just modify and argument in case of quotation marks. From "path with space" to path with space.
         *
         * @param[in,out] argument Argument which will be modified.
         */
        void changeArg( string & argument ) const;

        /**
         * Method will modify an string for command ls. Combines path to the current directory and argument.
         *
         * @param[in,out] arg Argument which will me modified.
         */
        void makePath( string & arg ) const;

        /**
         * Main method for executing a command.
         *
         * Method will parse the command from user, then react on that and call the proper method.
         *
         * @param[in] line Line which user entered.
         *
         * @return In case of success 0, in case of 10x invalid_command -2 and in case of close (the connection to the server) -1.
         */
        int treatCommand( const string & line );

        /**
         * This method will parse first word in line from user.
         *
         * @param[in] line Line which user entered.
         * @param[in,out] pos Position of the last letter of the word.
         *
         * @return String which represent command.
         */
        string parseLine( const string & line, int & pos ) const;

        /**
         * Main parsing method for arguments. This method parse a second word, or path from the line.
         *
         * @param[in] line Line which user entered.
         * @param[in,out] pos Position of the last letter of the second string.
         *
         * @return String which represent argument.
         */
        string parseArg( const string & line, int & pos ) const;

        /**
         * This method just clear a variables for next usage of the PASV.
         */
        void clearDataSocket();

        /**
         * This method will do everything necessary to execute a command cd.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandCD( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command ls.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandLS( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command get.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandGET( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command getd.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandGETD( const string & line, int pos ) const;

        /**
         * This method will do everything necessary to execute a command mkdir.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandMKDIR( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command rmdir.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandRMDIR( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command delete.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandDELETE( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command send.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandSEND( const string & line, int pos );

        /**
         * This method will do everything necessary to execute a command create.
         *
         * @param[in] line Line which user entered.
         * @param[in] pos Position of the last letter of the command in line.
         */
        void commandCREATE( const string & line, int pos );

    protected:
        /** The variable which represent if the connection is running. */
        bool m_running;

        /** The variable which represent if is the passive mode on. */
        bool m_passive;

        /** Socket for communication to the server. */
        int m_sock;

        /** Port which is set after command PASV. */
        int m_pasvport;

        /** Address which is set after command PASV. */
        string m_address;

        /** Buffer for receiving a responses from the server. */
        char m_buffer[ MAX_BUFFER_LENGTH + 1 ];

        /** Line which is received after command PASV. */
        string m_pasvrec;

        /** Current path to the directory on the server. */
        string m_dir;

        /** The variable which represent a count of the invalid commands. */
        int m_invalidCnt;

        /** Map which represent a structure of the current folder. */
        map< string , char > dirContent;
};

#endif //POLCAVOJ_CLIENT_H
