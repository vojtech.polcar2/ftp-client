#ifndef POLCAVOJ_FACTORY_H
#define POLCAVOJ_FACTORY_H

#include "client.h"
#include "constants.h"
#include <string>

/**
 * This class is just auxiliary. It creates clients and their connection to the FTP server.
 */


class CFactory{
    public:
    /** Static class, instance of this class cannot be create. */
        CFactory( void ) = delete;

    /**
     *  Method creates a connection to the FTP server and creates socket.
     *
     *  @param[in] address Address of FTP server.
     *
     *  @return In case of success, method returns instance of client, otherwise returns NULL.
     */
        static CClient * createClient ( const string address );

    /**
     * Method will properly delete instance of client and set a pointer to the NULL.
     *
     * @param[in,out] client - Instance for deleting.
     */
        static void deleteClient( CClient * & client );

};

#endif //POLCAVOJ_FACTORY_H
