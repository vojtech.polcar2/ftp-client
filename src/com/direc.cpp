#include "direc.h"

void CDirec::executeCommand(){
    /* cd into the actual folder */
    string toServer = SRV_LIST + m_actDir + "\r\n";
    CCd cwd ( TYPE_CLIENT, m_actDir, m_actDir, m_sock, false );
    cwd.executeCommand();

    send( m_sock, SRV_PASV, 6, 0 );
    treatPASV();

    /* hidden ls for checking downloadable files/folders */
    send( m_sock, toServer.c_str(), toServer.length(), 0 );
    CList ls( TYPE_CLIENT, m_pasvport, m_address, true, false );
    if( ls.connectToServer() ){
        m_status = false;
        return;
    }

    treatResponse();
    ls.executeCommand();
    treatResponse();

    dirContent = ls.getDirContent();

    clearDataSocket();

    if( m_mode )
        downloadDirectory();
    else
        removeDirectory();
}

void CDirec::removeDirectory(){
    string toServer = "";
    for( auto it = dirContent.begin(); it != dirContent.end(); ++it ) {
        if( it->second == 'f' ){
            toServer = SRV_DEL + it->first + "\r\n";
            send( m_sock, toServer.c_str(), toServer.length(), 0 );
            treatResponse();
        }
        else{

            CDirec dir( TYPE_CLIENT, m_path + '/' + it->first, m_actDir + '/' + it->first, m_sock, m_address, m_asking, false );
            dir.executeCommand();

            CCd cwd ( TYPE_CLIENT, m_actDir, m_actDir, m_sock, false );
            cwd.executeCommand();

            toServer = SRV_RMDIR + it->first + "\r\n";
            send(m_sock, toServer.c_str(), toServer.length(), 0);
            treatResponse();
        }
    }
}

void CDirec::downloadDirectory(){
    int flagForDownload = 0;
    string answer = "", toServer = "";

    for( auto it = dirContent.begin(); it != dirContent.end(); ++it ){
        if( m_asking ){
            cout << "Do you want to download ";
            it->second == 'f' ? cout << "file " << m_actDir << "/" << it->first : cout << "folder " << m_actDir << "/" << it->first;
            cout << " (yes/no)." << endl;
            getline( cin, answer );
            while( 1 ){
                flagForDownload = treatAnswer( answer );
                if( flagForDownload != -1 )
                    break;

                cout << "Wrong answer, please use yes or no." << endl;
                getline( cin, answer );
            }

            if( flagForDownload == 0 )
                continue;
        }
        if( it->second == 'f' ){
            send(m_sock, SRV_PASV, 6, 0);
            treatPASV();

            /* sending just name - we are in the correct folder  */
            toServer = SRV_GET + it->first + "\r\n";
            send( m_sock, toServer.c_str(), toServer.length(), 0 );

            //cout << m_path << "/" << it->first << endl;
            /* last argument is path to the downloads file */
            CGet get( TYPE_CLIENT, m_pasvport, m_address, m_path + '/' + it->first.c_str() );
            if( get.connectToServer() ){
                m_status = false;
                return;
            }

            treatResponse();
            get.executeCommand();
            treatResponse();

            clearDataSocket();
        }
        else{
            string path = DOWNL_DIR + m_path + "/" + it->first;
            mkdir( path.c_str(), 0744);

            CDirec dir( TYPE_CLIENT, m_path + '/' + it->first, m_actDir + '/' + it->first, m_sock, m_address, m_asking, true );
            dir.executeCommand();

            if( ! dir.getStatus() )
                m_status = false;

            CCd cwd ( TYPE_CLIENT, m_actDir, m_actDir, m_sock, false );
            cwd.executeCommand();
        }
    }
}

int CDirec::treatAnswer( const string & line ) const {
    string res = "";
    for( size_t i = 0; i < line.length(); ++i ){
        if( res != "" && ( line[i] == ' ' || line[i] == '\t' ) ) {
            break;
        }

        if(  line[i] == ' ' || line[i] == '\t' )
            continue;
        else
            res += line[i];
    }

    if( res == "y" || res == "yes" )
        return 1;
    else if( res == "n" || res == "no" )
        return 0;
    else
        return -1;
}

void CDirec::clearDataSocket(){
    m_address = "";
    m_pasvrec = "";
    m_pasvport = 0;
}

int CDirec::calcPASV(){
    bool flag = true;
    string number1 = "", number2 = "", address = "";
    int symbols = 4, num1 = 0, num2 = 0;
    for( size_t i = 0; i < m_pasvrec.length(); ++i ){
        if( flag && m_pasvrec[i] != '(')
            continue;
        else
            flag = false;

        if( m_pasvrec[i] == ')' )
            break;

        if( m_pasvrec[i] == ',' ) {
            symbols--;
            if( symbols > 0 )
                address += '.';
            continue;
        }

        if( symbols == 0)
            number1 += m_pasvrec[i];

        else if( symbols == -1 )
            number2 += m_pasvrec[i];
        else if( symbols > 0 && m_pasvrec[i] != '(' && m_pasvrec[i] != ',' )
            address += m_pasvrec[i];

    }

    num1 = atoi(number1.c_str() );
    num2 = atoi(number2.c_str() );

    m_address = address;

    return ( (num1 * 256) + num2 );
}

void CDirec::treatPASV(){
    bool flag = false;
    int cntBytes;
    char previousByte, actualByte;
    string text = "";
    m_pasvrec = "";
    while (1) {
        cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0);

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                m_pasvrec += text;
                text = "";
                flag = true;
            }
            else
                text += m_buffer[i];

            previousByte = actualByte;
        }

        if ( flag ) {
            flag = false;
            break;
        }
    }
    m_pasvport = calcPASV();
}

bool CDirec::getStatus() const {
    return m_status;
}

void CDirec::treatResponse(){
    bool flag = false;
    int cntBytes;
    char previousByte, actualByte;
    while (1) {
        cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0);

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                flag = true;
            }
            previousByte = actualByte;
        }

        if ( flag ) {
            flag = false;
            break;
        }
    }
}