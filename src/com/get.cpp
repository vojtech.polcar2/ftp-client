#include "get.h"

CGet::~CGet(){
    close(m_sock);
}

void CGet::treatResponse(){
    int cntBytes;
    char previousByte, actualByte;
    char m_buffer[ MAX_BUFFER_LENGTH + 1 ];
    string text = "", line = "";
    ofstream out( DOWNL_DIR + m_fname, ios::out | ios::binary );
    while (1) {
        if( ( cntBytes = recv(m_sock, m_buffer, MAX_BUFFER_LENGTH, 0) ) <= 0 )
            break;

        for (int i = 0; i < cntBytes; ++i) {
            actualByte = m_buffer[i];
            if (actualByte == '\n' && previousByte == '\r') {
                out.write((char *)text.c_str(), text.length());
                m_size += text.length();
                out.write((char *)"\n", 1);
                text = "";
            }
            else {
                text += m_buffer[i];
                line += m_buffer[i];
            }

            previousByte = actualByte;
        }
    }
    out.close();
}

void CGet::executeCommand(){
    treatResponse();
    cout << *this;
    send( m_sock, SRV_QUIT, 6, 0);
}

void CGet::print( ostream & os ) const{
    os << m_size << " bytes received." << endl;
}

int CGet::connectToServer(){
    struct addrinfo * ai;

    string port = to_string(m_port);

    if ( getaddrinfo( m_server.c_str(), port.c_str(), NULL, &ai ) != 0 ) {
        cerr << "Error during getaddrinfo()." << endl;
        return 1;
    }

    m_sock = socket ( ai -> ai_family, SOCK_STREAM, 0 );
    if ( m_sock == -1 ){
        freeaddrinfo ( ai );
        cerr << "Error during socket()." << endl;
        return 1;
    }

    if ( connect ( m_sock, ai -> ai_addr, ai -> ai_addrlen ) != 0 ){
        close ( m_sock );
        freeaddrinfo ( ai );
        cerr << "Error during connect()." << endl;
        return 1;
    }

    freeaddrinfo ( ai );
    return 0;
}