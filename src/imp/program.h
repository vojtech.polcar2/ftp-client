#ifndef POLCAVOJ_PROGRAM_H
#define POLCAVOJ_PROGRAM_H

#include "../pom/factory.h"
#include "../com/help.h"

using namespace std;

/**
 * Main class which represent an FTP client.
 *
 * You can make as many connections to the FTP server as you want.
 */

class CProgram{
    public:
        /**
         * Constructor which start program and immediately creates connection to FTP server.
         *
         * @param[in] server Address of FTP server.
         */
        CProgram( const string & server );

        /**
         * Main method. Program runs whole time there. When we leave this method, program will end.
         */
        void work();

        /**
         * Method which parse a command from standard input and decides what should be done.
         *
         * @param[in] line Line from standard input, which si made by getline().
         *
         * @return Method returns zero if everything is ok, and -1 if a command is exit.
         */
        int treatCommand( const string & line );

        /**
         * Method which connect program to the FTP server. Method must parse a name of server from rest of the line.
         *
         * @param[in] line Line from which must be parsed a name of the server.
         * @param[in] pause Position on the line where command ends.
         */
        void connect( const string & line, int pause );

        /**
         * Method which parse a command from standard input.
         *
         * @param[in] line Line from which must be parsed a command.
         * @param[in] pos Position on the line. It's always zero, but for method connect, we need to remember where the command ends.
         *
         * @return It's a string with command to execute.
         */
        string parseLine( const string & line, int & pos ) const;

    private:
        /**
         * Pointer to the connection to the FTP server.
         */
        CClient * m_client;

        /**
         * The name of the server, to which is the program currently connected.
         */
        string m_server;

        /**
         * Count of the Invalid commands.
         */
        int m_invalidCnt;
};

#endif //POLCAVOJ_PROGRAM_H
