#ifndef POLCAVOJ_LIST_H
#define POLCAVOJ_LIST_H

#include "../imp/command.h"

#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <map>

using namespace std;

/**
 * This class is used for display items in folder. It's descendant of CCommand.
 *
 * In case of command without argument ("ls"), class creates a copy of a structure in the folder and save it for possible download.
 */

class CList : public CCommand{
    public:
        /**
         * Constructor in which is called constructor of parent and some variables, which are need for communication with server.
         *
         * @param[in] type Type of mode in which is program currently running.
         * @param[in] port Port for data socket.
         * @param[in] server Address of server.
         * @param[in] signal Variable which decides if a structure of a folder will be saved for later.
         * @param[in] visible Variable which decides if will be an output displayed to the command line.
         */
        CList( const string & type, int port, const string & server, bool signal, bool visible ) : CCommand(type), m_port(port), m_sock(0),
                                                                                                   m_server(server), m_signal(signal), m_visible(visible){}

        /**
         * Desctuctor which will close socket in the end.
         */
        virtual ~CList();

        /**
         * Main method in class. Method just controls and calls other methods.
         */
        virtual void executeCommand();

        /**
         * Method which print to the command line.
         *
         * @param[in,out] os Ostream which print a number of files/folders in the structure.
         */
        virtual void print( ostream & os ) const;

        /**
         *  Method which will connect class with an FTP server on data socket. From this socket are downloaded all data.
         *
         *  @return In case of success method returns zero, otherwise returns 1. Socket is saved in the local variable m_sock.
         */
        int connectToServer();

        /**
         * Method will treat a response from server. If is m_visible true, method will print output data to the command line.
         */
        void treatResponse();

        /**
         * Method which returns structure of the folder.
         *
         * @return Map of structure of the folder.
         */
        map< string, char > getDirContent() const;

        /**
         * Method which parse line from an data socket and saved name of the item and type to the map.
         *
         * @param[in] line One line from data socket.
         */
        void parseLine( const string & line );

    protected:
        /** Number of a port to the data socket. */
        int m_port;

        /** Variable which represent data socket. */
        int m_sock;

        /** Name of the server. */
        string m_server;

        /** Variable which decides if the folder structure will be saved. */
        bool m_signal;

        /** Variable which decides if a data output will be displayed to the command line. */
        bool m_visible;

        /** Map of the structure of a folder. */
        map< string, char > dirContent;

};

#endif //POLCAVOJ_LIST_H
