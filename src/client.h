#ifndef POLCAVOJ_CLIENT_H
#define POLCAVOJ_CLIENT_H

#include "constants.h"

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

using namespace std;

class CClient{
    public:
        CClient( int sock ) : m_running(true), m_sock(sock){}
        ~CClient( void );
        bool isRunning( void );
        void communicate();


    protected:
        bool m_running;
        int m_sock;

        char m_buffer[ MAX_BUFFER_LENGTH + 1 ];
        bool authenticate();
        void treatResponse( );

};

#endif //POLCAVOJ_CLIENT_H
