#include "help.h"

void CHelp::executeCommand(){
    cout << *this;
}

void CHelp::print( ostream & os ) const{
    if( m_type == TYPE_PROG ){
        os << "---------------------------------------------------------------------------" << endl;
        os << "All arguments could be with or without quotation marks." << endl;
        os << "open <server_address> - Open connection to FTP server." << endl;
        os << "exit - Exit program." << endl;
        os << "help - Display all available commands in current mode." << endl;
        os << "---------------------------------------------------------------------------" << endl;
    }
    else if( m_type == TYPE_CLIENT ){
        os << "---------------------------------------------------------------------------" << endl;
        os << "All arguments could be with or without quotation marks." << endl;
        os << "pasv - Set the mode for data transfer. Important!" << endl;
        os << "close - Close connection to FTP server." << endl;
        os << "ls - Display list of files in current/specified folder." << endl;
        os << "cd <path> or cd <path> - Change working directory." << endl;
        os << "mkdir <name> - Create a directory. It could be called as <name> or <path/name>" << endl;
        os << "rmdir <name> - Remove a directory. It could be called as <name> or <path/name>. Possible argument -R: rmdir -R <dirname>" << endl;
        os << "delete <name> - Delete a file. It could be called as <name> or <path/name>." << endl;
        os << "get <name> - Download a file in current folder." << endl;
        os << "getd <dirname> - Recursively download a folder from current folder. Possible argument -A: getd -A <dirname>" << endl;
        os << "create <name> - Create an empty file." << endl;
        os << "send <path/name> <name-on-server> - Send a file to the FTP server." << endl;
        os << "---------------------------------------------------------------------------" << endl;
    }
}