#include "send.h"

void CSend::emptyFile(){
    ofstream of( "/tmp/" + m_fname, ios::out | ios::binary );
    of.close();

    char m_buffer[ MAX_BUFFER_LENGTH + 1 ];
    ifstream f ( "/tmp/" + m_fname, ios::in | ios::binary );
    while ( ! f.eof() ) {
        f.read((char *)&m_buffer, MAX_BUFFER_LENGTH);
        m_size += f.gcount();

        send( m_sock, m_buffer, f.gcount() , 0 );
    }
    f.close();
}

void CSend::sendFile() {
    struct stat s;
    if( stat(m_fname.c_str(),&s) == 0 ){
        if( s.st_mode & S_IFDIR ){
            cerr << m_fname << ERROR_FOLDER << endl;
            m_error = true;
            return;
        }
        else if( s.st_mode & S_IFREG ){
            char m_buffer[MAX_BUFFER_LENGTH + 1];
            ifstream f( m_fname, ios::in | ios::binary );
            if( !f.good() ){
                cerr << ERROR_EXIST << endl;
                m_error = true;
                f.close();
                return;
            }

            while ( ! f.eof() ) {
                f.read((char *)&m_buffer, MAX_BUFFER_LENGTH);
                m_size += f.gcount();

                send( m_sock, m_buffer, f.gcount(), 0 );
            }
            f.close();
        }
        else{
            cerr << m_fname << ERROR_FILE_FOLDER << endl;
            m_error = true;
            return;
        }
    }
    else{
        cerr << m_fname << ERROR_EXIST << endl;
        m_error = true;
        return;
    }
}

bool CSend::getError() const{
    return m_error;
}

void CSend::executeCommand(){
    if( m_mode )
        emptyFile();
    else
        sendFile();

    cout << *this;
    close( m_sock );
}

void CSend::print( ostream & os ) const{
    os << m_size << " bytes sent." << endl;
}

int CSend::connectToServer(){
    struct addrinfo * ai;

    string port = to_string(m_port);

    if ( getaddrinfo( m_server.c_str(), port.c_str(), NULL, &ai ) != 0 ) {
        cerr << "Error during getaddrinfo()." << endl;
        return 1;
    }

    m_sock = socket ( ai -> ai_family, SOCK_STREAM, 0 );
    if ( m_sock == -1 ){
        freeaddrinfo ( ai );
        cerr << "Error during socket()." << endl;
        return 1;
    }

    if ( connect ( m_sock, ai -> ai_addr, ai -> ai_addrlen ) != 0 ){
        close ( m_sock );
        freeaddrinfo ( ai );
        cerr << "Error during connect()." << endl;
        return 1;
    }

    freeaddrinfo ( ai );
    return 0;
}